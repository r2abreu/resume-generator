import lume from "lume/mod.ts";
import sass from "lume/plugins/sass.ts";

const site = lume({
  src: "./src",
});

site.copy([".css", ".svg"]);
site.use(sass());

export default site;
